import React, {useState} from "react";
import Header from "./Header.jsx";
import Footer from "./Footer.jsx";
import Note from "./Note.jsx";
import CreateArea from "./CreateArea.jsx";
import notes from "../notes.js";


function App() {
    const [notes, setNotes] = useState([]);


    function addNote(note) {
        setNotes([
            ...notes,
            note
        ]);
    }

    function deleteNote(id) {        
        setNotes(notes.filter((note, index) => index !== id));
    }

    return (
        <div>
            <Header />
            <CreateArea onClick={addNote}/>
            {notes.map((note, index) => <Note onDelete={deleteNote} key={index} id={index} title={note.title} content={note.content}/>)}
            <Footer />
        </div>
    );
}

export default App;
