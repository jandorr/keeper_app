import React, {useState} from "react";
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Zoom from '@material-ui/core/Zoom';


function CreateArea(props) {
    const [note, setNote] = useState({
        title: "",
        content: ""
    });
    const [isExpanded, setExpanded] = useState(false);

    function onChange(event) {
        const {name, value} = event.target;

        setNote({
            ...note,
            [name]: value
        });
    }

    function onSubmit(event) {
        props.onClick(note);
        setNote({
            title: "",
            content: ""
        });
        event.preventDefault();
    }

    function expand() {
        setExpanded(true);
    }

    return (
        <div>
            <form className="create-note">
            {isExpanded && (
                <input onChange={onChange} name="title" value={note.title} placeholder="Title"/>
            )}
            <textarea 
                onChange={onChange}
                onClick={expand}
                name="content"
                placeholder="Take a note..."
                rows={isExpanded ? 3 : 1}
                value={note.content}
            />
            <Zoom in={isExpanded}>
            <Fab onClick={onSubmit}>
                <AddIcon />
            </Fab>
            </Zoom>
            </form>
        </div>
    );
}

export default CreateArea;
